<?php

namespace App\Http\Controllers;

use App\Shops;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Auth;
class ShopsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = Shops::all();
        return view("shop/index")->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("shop/create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     
    public function store(Request $request)
    {
        $rules = array(
            'name'       => 'required|email',
            'description'      => 'required',
            'subtitle' => 'required'
        );
        
        $request->validate($rules);
        
        $shop = new Shops();
        $shop->name  = $request->name;
        $shop->subtitle  = $request->subtitle;
        $shop->description  = $request->description;
        $shop->save();

        return Redirect::to("shop");

        /*  Another way to use validate /*
        $validator = Validator::make(Input::all(), $rules);  
        
        if ($validator->fails()) {
            return Redirect::to('shop/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $shop = new Shops();
            $shop->name  = $request->name;
            $shop->subtitle  = $request->subtitle;
            $shop->description  = $request->description;
            $shop->save();

            return Redirect::to("shop");
        }
        */

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Shops::findorFail($id);
        return view("shop/show")->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Shops::findorFail($id);
        return view("shop/edit")->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shops  $shops
     * @return \Illuminate\Http\Response
     */

     
    public function update(Request $request, Shops $shops, $id)
    {
        $messages = [
            'subtitle.required' => 'We need to know your subtitle address!',
        ];
        $shop = Shops::findorFail($id);

        $rules = array(
            'name'       => 'required|email',
            'description'      => 'required',
            'subtitle' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules,$messages);

        
        if ($validator->fails()) {
            return Redirect::to('shop/'.$id.'/edit')
                ->withErrors($validator)
                ->withInput();
        } else {
            
            $shop->name  = $request->name;
            $shop->subtitle  = $request->subtitle;
            $shop->description  = $request->description;
            $shop->save();

            return Redirect::to("shop");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shops  $shops
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shop = Shops::findorFail($id);
        $shop->delete();
        return Redirect::back();
    }

    
}
