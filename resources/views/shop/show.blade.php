@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Shop</div>
                
                <div class="card-body">

               

                <table class="table table-hover table-bordered">
                    <tr>
                        <th>Name</th>
                        <td>{{$data->name}} </td>
                        
                    </tr>
                    
                    <tr>
                        <th>Subtitle</th>
                        <td>{{$data->subtitle}} </td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td>{{$data->description}} </td>
                    </tr>
                    
                </table>
               
               
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
