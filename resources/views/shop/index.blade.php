@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Shop</div>
                
                <div class="card-body">

               

                <table class="table table-hover table-bordered">
                    <tr>
                        <th>Name</th>
                        <th>Subtitle</th>
                        <th></th>
                    </tr>
                    @foreach($data as $shop)
                    <tr>
                        <td>{{$shop->name}} </td>
                        <td>{{$shop->subtitle}} </td>
                        <td>
                            <a href="{{ url('shop/'.$shop->id.'/edit') }}" class="btn btn-primary a-btn-slide-text">
                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                <span><strong>Edit</strong></span>            
                            </a>
                            
                            <a href="{{ url('shop/'.$shop->id) }}" class="btn btn-primary a-btn-slide-text">
                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                <span><strong>View</strong></span>            
                            </a>
                            <form action="{{ url('shop/'.$shop->id) }}" method="POST" style="display:inline">
                            @csrf
                            {{method_field('DELETE')}}
                            <input type="hidden" value="{{ $shop->id }}" />

                                <input href="#" type="submit" value="Delete" class="btn btn-primary a-btn-slide-text" onclick="return confirm('Are you sure you want to delete ?')">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                              
                                </a> 
                            </form>
                        
                        </th>
                    </tr>
                    @endforeach
                </table>
                <div align="right">
                            <a href="{{ url('shop/create') }} " class="btn btn-primary a-btn-slide-text">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                <span><strong>Add</strong></span>            
                            </a>
                </div>
                
               
                    <!--
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
