<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Route::group( [ 'prefix' => 'admin' ], [ 'middleware' => 'auth', function()

//To add the resource controller , write
//  Route::resource('shop', 'ShopsController');  // Abdulla aldoseri


//This line will allow update/edit/destroy/create/store to reqiure login
Route::group(['middleware' => 'auth'], function()
{

    Route::resource('shop', 'ShopsController', [
        'only' => ['update', 'edit','destroy','create','store']
    ]);
    
});

//This line allow index and show to be viewed without login.
Route::resource('shop', 'ShopsController', [
    'only' => ['index', 'show']
]);


Route::resource('user', 'UserController');